package fi.oulu.tol.esde_2017_021.cwpclient021;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPControl;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolListener;

public class ControlFragment extends Fragment implements Observer {

    private ToggleButton cwpbtn;
    private EditText editFreq;
    private boolean isChecked = false;
    private SharedPreferences sharedPrefs;

    private CWPControl cwpctrl;

    public ControlFragment() {
        // Required empty public constructor
    }

    public void setControl(CWPControl ctrl) {
        cwpctrl = ctrl;
        cwpctrl.addObserver(this);
        if (getView() != null) {
            updateView(getView());
        }
    }

    private void updateView(View view) {
        final ToggleButton toggle = (ToggleButton) view.findViewById(R.id.connectCWP);
        if (cwpctrl.isConnected()) {
            Log.d("ControlFragment", "toggle.setChecked(true)");
            isChecked = true;
            toggle.setChecked(true);
        } else {
            Log.d("ControlFragment", "toggle.setChecked(false)");
            isChecked = false;
            toggle.setChecked(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        View rootView = inflater.inflate(R.layout.fragment_control, container, false);
        cwpbtn = (ToggleButton) rootView.findViewById(R.id.connectCWP);
        Button selectBtn = (Button) rootView.findViewById(R.id.select_button);
        editFreq = (EditText) rootView.findViewById(R.id.edit_frequency);
        editFreq.setGravity(Gravity.CENTER_HORIZONTAL);
        int negativeFreq = -Math.abs(Integer.parseInt(sharedPrefs.getString("frequency_setting", "-1")));
        editFreq.setText(String.valueOf(negativeFreq), TextView.BufferType.EDITABLE);

        cwpbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (cwpctrl == null) {
                        cwpbtn.setChecked(false);
                        isChecked = false;
                    } else {
                        if (!isChecked) {
                            EventLogger.logEventStarted("Connected started.", System.currentTimeMillis());
                            cwpbtn.setChecked(true);
                            isChecked = true;
                            cwpctrl.connect(sharedPrefs.getString("server_setting", "cwp.opimobi.com"),
                                    Integer.parseInt(sharedPrefs.getString("port_setting", "20000")),
                                    Integer.parseInt(sharedPrefs.getString("frequency_setting", "-1")));
                        } else {
                            EventLogger.logEventStarted("Disconnected started.", System.currentTimeMillis());
                            cwpbtn.setChecked(false);
                            isChecked = false;
                            cwpctrl.disconnect();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                if (cwpctrl != null) {
                    if (cwpctrl.isConnected()) {
                        String input = editFreq.getText().toString();
                        if (input.equals("") || input.equals("0")) {
                            editFreq.setText(String.valueOf(-Math.abs(Integer.parseInt(sharedPrefs.getString("frequency_setting", "-1")))), TextView.BufferType.EDITABLE);
                            Toast.makeText(getActivity(), "Enter frequency other than zero!", Toast.LENGTH_SHORT).show();
                        } else {
                            int newFreq = -Math.abs(Integer.parseInt(editFreq.getText().toString()));
                            editFreq.setText(String.valueOf(newFreq), TextView.BufferType.EDITABLE);
                            if (cwpctrl.frequency() != newFreq) {
                                try {
                                    EventLogger.logEventStarted("ChangedFrequency started.", System.currentTimeMillis());
                                    cwpctrl.setFrequency(newFreq);
                                    SharedPreferences.Editor edit = sharedPref.edit();
                                    edit.putString("frequency_setting", Integer.toString(newFreq));
                                    edit.apply();
                                    Log.d("CTRLFRAG", "setFrequency(" + newFreq);
                                } catch (IOException | InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.d("CTRLFRAG", "No change in frequency.");
                            }
                        }
                    }
                } else {
                    editFreq.setText(sharedPrefs.getString("frequency_setting", "-1"), TextView.BufferType.EDITABLE);
                    Toast.makeText(getActivity(), "Connect to a server first!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (cwpctrl != null) {
            cwpctrl = ((CWPProvider) getActivity()).getControl();

            if (context instanceof CWPProvider) {
                cwpctrl.addObserver(this);
            }
        }
    }

    @Override
    public void onDetach() {
        if (cwpctrl != null) {
            Context context = getContext();
            if (context instanceof CWPProvider) {
                cwpctrl.deleteObserver(this);
            }
            cwpctrl = null;
        }
        super.onDetach();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg == CWProtocolListener.CWPEvent.EConnected) {
            Toast.makeText(getActivity(), "Connected: " + sharedPrefs.getString("server_setting", "cwp.opimobi.com"), Toast.LENGTH_SHORT).show();
            EventLogger.logEventEnded("Connected ended.");
        }
        if (arg == CWProtocolListener.CWPEvent.EDisconnected) {
            cwpbtn.setChecked(false);
            isChecked = false;
            Toast.makeText(getActivity(), "Disconnected.", Toast.LENGTH_SHORT).show();
            EventLogger.logEventEnded("Disconnected ended.");
        }
        if (arg == CWProtocolListener.CWPEvent.EChangedFrequency) {
            Toast.makeText(getActivity(), "Current frequency: " + sharedPrefs.getString("frequency_setting", "-1"), Toast.LENGTH_SHORT).show();
            EventLogger.logEventEnded("ChangedFrequency ended.");
        }
    }
}
