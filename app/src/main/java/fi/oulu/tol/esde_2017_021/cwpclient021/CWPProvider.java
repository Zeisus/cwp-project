package fi.oulu.tol.esde_2017_021.cwpclient021;

import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPControl;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPMessaging;

public interface CWPProvider {
    CWPMessaging getMessaging();
    CWPControl getControl();
}