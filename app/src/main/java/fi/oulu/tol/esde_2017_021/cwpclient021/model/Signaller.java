package fi.oulu.tol.esde_2017_021.cwpclient021.model;

import android.media.AudioManager;
import android.media.ToneGenerator;

import java.util.Observable;
import java.util.Observer;

import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolImplementation;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolListener;

class Signaller implements Observer {

    private final ToneGenerator toneGen = new ToneGenerator(AudioManager.STREAM_DTMF, 80);

    @Override
    public void update(Observable o, Object arg) {
        if (arg == CWProtocolImplementation.CWPState.LineUp || arg == CWProtocolListener.CWPEvent.ELineUp) {
            start();
        } else {
            stop();
        }
    }

    private void start() {
        toneGen.startTone(AudioManager.STREAM_DTMF);
    }

    private void stop() {
        toneGen.stopTone();
    }


}
