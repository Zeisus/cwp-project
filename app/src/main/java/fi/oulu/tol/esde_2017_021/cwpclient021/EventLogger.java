package fi.oulu.tol.esde_2017_021.cwpclient021;

import android.util.Log;

class EventLogger {

    private static final String TAG = "CWPLogger";
    private static long eventStarted = 0;

    public static void logEventStarted(String event, long timeStamp) {
        eventStarted = timeStamp;
        Log.d(TAG, event);
    }

    public static void logEventEnded(String event) {
        if (eventStarted > 0) {
            long duration = System.currentTimeMillis() - eventStarted;
            Log.d(TAG, event + " duration: " + Integer.toString((int) duration));
        }
    }

}