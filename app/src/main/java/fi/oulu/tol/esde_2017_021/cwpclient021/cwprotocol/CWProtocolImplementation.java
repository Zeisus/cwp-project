package fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Observer;
import java.util.concurrent.Semaphore;

public class CWProtocolImplementation implements CWPControl, CWPMessaging, Runnable {

    private static final String TAG = "CWPImplementation";
    public enum CWPState { Disconnected, Connected, LineUp, LineDown }
    private CWPState currentState = CWPState.Disconnected;
    private int currentFrequency = DEFAULT_FREQUENCY;
    private CWPConnectionReader cwpcr = null;
    private final Handler receiveHandler = new Handler(); // <<< The Handler used to tell to post events to protocol.
    private CWPState nextState = null;
    private CWPState previousState;
    private int messageValue = 0;
    private CWProtocolListener cwplistener = null;
    private static final int BUFFER_LENGTH = 64;
    private OutputStream nos = null; //Network Output Stream
    private String serverAddr = null;
    private int serverPrt = 20000;
    private long connectTime = 0;
    private long lineUpTime = 0;
    private boolean serverLineUp = false;
    private boolean userLineUp = false;
    private final Semaphore lock = new Semaphore(1);

    private class CWPConnectionReader extends Thread {

        private boolean running = false;
        private Runnable myProcessor = null;
        private static final String TAG = "CWPReader";
        private Socket cwpSocket = new Socket();
        private InputStream nis = null; //Network Input Stream
        int bytesRead = 0;


        CWPConnectionReader(Runnable processor) {
            myProcessor = processor;
        }

        private int readLoop(byte [] bytes, int bytesToRead) throws IOException {
            do {
                int readNow = nis.read(bytes, bytesRead, bytesToRead - bytesRead);
                if (readNow == -1) { // end of stream, server closed the connection.
                    throw new IOException("Read -1 from stream");
                }
                bytesRead = readNow;
            } while (bytesToRead > bytesRead);
            //Log.d(TAG, "Read " + bytesRead + " bytes.");
            return bytesRead;
        }

        void startReading() {
            running = true;
            this.start();
        }

        void stopReading() {
            try {
                Log.d(TAG, "stopReading called!");
                running = false;
                cwpSocket.close();
                nis.close();
                nos.close();
                nis = null;
                nos = null;
                cwpSocket = null;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void doInitialize() throws InterruptedException, IOException {
            InetSocketAddress socketAddress = new InetSocketAddress(serverAddr, serverPrt);
            Socket cwpSocket = new Socket();
            cwpSocket.connect(socketAddress, 5000);
            connectTime = System.currentTimeMillis();
            nis = cwpSocket.getInputStream();
            nos = cwpSocket.getOutputStream();
            this.changeProtocolState(CWPState.Connected, 2);
        }

        private void changeProtocolState(CWPState state, int param) throws InterruptedException {
            previousState = nextState;
            nextState = state;
            messageValue = param;
            lock.acquire();
            receiveHandler.post(myProcessor); // <<<< tell the android.os.Handler to post an event to myProcessor.
        }

        @Override
        public void run() {
            try {
                doInitialize();
                byte[] byteArr = new byte[BUFFER_LENGTH];
                ByteBuffer byteBuff = ByteBuffer.wrap(byteArr);
                byteBuff.order(ByteOrder.BIG_ENDIAN);
                // continue preparations for reading data from server
                // after preparations, read data in a while loop:
                while (running) {
                    if (readLoop(byteArr, 4) == 4) {
                        bytesRead = 0;
                        int intValue = byteBuff.getInt(0);
                        byteBuff.clear().position(0);
                        // if int value of the received message is negative
                        // we got the default frequency message
                        if (intValue < 0) {
                            changeProtocolState(CWPState.LineDown, intValue);
                            serverLineUp = false;
                        }
                        // if int value is positive
                        // we got a line up message
                        if (intValue > 0) {
                            changeProtocolState(CWPState.LineUp, intValue);
                            serverLineUp = true;
                            // we get 2 bytes AFTER a line up message
                            // we got a line down message
                            if (readLoop(byteArr, 2) == 2) {
                                bytesRead = 0;
                                serverLineUp = false;
                                // we change protocol state to LineDown only if
                                // user is not sending line up signal
                                if (!userLineUp) {
                                    changeProtocolState(CWPState.LineDown, intValue);
                                }

                            }
                        }
                    }
                }
            }
            catch (IOException | InterruptedException e) {
                e.printStackTrace();
                // in case of internet connection failure we want to set the protocol state to
                // Disconnected in order to avoid operations on null socket objects and to keep
                // the state of the app coherent.
                try {
                    changeProtocolState(CWPState.Disconnected, 3);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public CWProtocolImplementation(CWProtocolListener listener_p) {
        cwplistener = listener_p;
    }

    public CWPState getCurrentState() {
        try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return CWPState.Disconnected;
        } finally {
            lock.release();
        }
        return currentState;
    }

    private void sendMessage(int msg) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.putInt(msg);
        bb.position(0);
        byte [] byteArr = bb.array();
        nos.write(byteArr);
        nos.flush();
    }

    private void sendMessage(short msg) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.putShort(msg);
        bb.position(0);
        byte [] byteArr = bb.array();
        nos.write(byteArr);
        nos.flush();
    }

    private void sendFrequency(int frequency) throws InterruptedException, IOException {
        lock.acquire();
        if (currentState != CWPState.LineDown) {
            lock.release();
            cwpcr.changeProtocolState(CWPState.LineDown, frequency);
            Log.d(TAG, "Current state not lineDown. Changing to lineDown...");
        } else {
            Log.d(TAG,"Sending frequency message to server.");
            sendMessage(frequency);
            lock.release();
            cwpcr.changeProtocolState(CWPState.Connected, frequency);
        }
    }

    @Override
    public void run() {
        switch (nextState) {
            case Connected: {
                Log.d(TAG, "State change to connected happening...");
                currentState = nextState;
                lock.release();
                cwplistener.onEvent(CWProtocolListener.CWPEvent.EConnected, messageValue); // <<<< Listener gets notified!
                break;
            }
            case Disconnected: {
                Log.d(TAG, "State change to disconnected happening...");
                currentState = nextState;
                lock.release();
                cwplistener.onEvent(CWProtocolListener.CWPEvent.EDisconnected, messageValue);
                break;
            }
            case LineUp: {
                Log.d(TAG, "State change to lineup happening...");
                currentState = nextState;
                lock.release();
                cwplistener.onEvent(CWProtocolListener.CWPEvent.ELineUp, messageValue);
                break;
            }
            case LineDown: {
                lock.release();
                if (previousState == CWPState.Connected) {
                    currentState = nextState;
                    if (frequency() != messageValue) {
                        try {
                            sendFrequency(currentFrequency);
                        } catch (InterruptedException | IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d(TAG, "Already on the correct frequency.");
                        cwplistener.onEvent(CWProtocolListener.CWPEvent.EChangedFrequency, messageValue);
                    }
                }
                Log.d(TAG, "State change to linedown happening...");
                currentState = nextState;
                lock.release();
                cwplistener.onEvent(CWProtocolListener.CWPEvent.ELineDown, messageValue);
                break;

            }
        }
    }

    @Override
    public void addObserver(Observer observer) {

    }

    @Override
    public void deleteObserver(Observer observer) {

    }

    @Override
    public void connect(String serverAddress, int serverPort, int frequency) throws IOException {
        serverAddr = serverAddress;
        serverPrt = serverPort;
        currentFrequency = -Math.abs(frequency);
        cwpcr = new CWPConnectionReader(this);
        cwpcr.startReading();
        currentState = CWPState.Connected;
    }

    @Override
    public void disconnect() throws IOException {
        if (cwpcr != null) {
            cwpcr.stopReading();
        }
        serverAddr = null;
        serverPrt = 0;
        currentFrequency = DEFAULT_FREQUENCY;
        cwpcr = null;

    }

    @Override
    public boolean isConnected() {
        boolean isConnected = false;
        try {
            lock.acquire();
            if (!(currentState == CWPState.Disconnected)) {
                isConnected = true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.release();
        }
        return isConnected;
    }

    @Override
    public void setFrequency(int frequency) throws IOException, InterruptedException {
        if (getCurrentState() != CWPState.LineDown) {
            cwpcr.changeProtocolState(CWPState.LineDown, frequency);
        }
        currentFrequency = -Math.abs(frequency);
        sendFrequency(currentFrequency);
        cwpcr.changeProtocolState(CWPState.Connected, frequency);
    }

    @Override
    public int frequency() {
        return currentFrequency;
    }

    @Override
    public void lineUp() throws IOException {
        try {
            lock.acquire();
            if (currentState == CWPState.LineUp || currentState == CWPState.LineDown) {
                sendMessage((int) (System.currentTimeMillis() - connectTime));
                lineUpTime = System.currentTimeMillis();
                try {
                    cwpcr.changeProtocolState(CWPState.LineUp, 1);
                    userLineUp = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.release();
        }
    }

    @Override
    public void lineDown() throws IOException {
        try {
            lock.acquire();
            if (currentState == CWPState.LineUp || currentState == CWPState.LineDown) {
                sendMessage((short) (System.currentTimeMillis() - lineUpTime));
                userLineUp = false;
                if (!serverLineUp) {
                    try {
                        cwpcr.changeProtocolState(CWPState.LineDown, 0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.release();
        }
    }

    @Override
    public boolean lineIsUp() {
        boolean lineIsUp = false;
        try {
            lock.acquire();
            if (currentState == CWPState.LineUp) {
                lineIsUp = true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.release();
        }
        return lineIsUp;
    }
}
