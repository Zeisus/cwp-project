package fi.oulu.tol.esde_2017_021.cwpclient021;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPMessaging;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolListener;


public class TappingFragment extends Fragment implements Observer {

    private ImageView iv;
    private CWPMessaging cwpmsg = null;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TappingFragment.
     */

    public TappingFragment() {
        // Required empty public constructor
    }

    public void setMessaging(CWPMessaging msg) {
        Log.d("TappingFragment", "Got interface to messaging");
        cwpmsg = msg;
        cwpmsg.addObserver(this);
        if (getView() != null) {
            updateView(getView());
        }
    }

    private void updateView(View view) {
        ImageView tapImage = (ImageView) view.findViewById(R.id.tappingImg);
        if (cwpmsg.isConnected()) {
            if (cwpmsg.lineIsUp()) {
                tapImage.setImageResource(R.mipmap.hal9000_up);
                Log.d("TappingFragment", "updateView set image to lineup");
            } else {
                tapImage.setImageResource(R.mipmap.hal9000_down);
                Log.d("TappingFragment", "updateView set image to linedown");
            }
        } else {
            tapImage.setImageResource(R.mipmap.hal9000_offline);
            Log.d("TappingFragment", "updateView set image to offline");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tapping, container, false);
        iv = (ImageView) rootView.findViewById(R.id.tappingImg);
        iv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (cwpmsg != null) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            cwpmsg.lineUp();
                            EventLogger.logEventStarted("LineUp started.", System.currentTimeMillis());
                        }
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            cwpmsg.lineDown();
                            EventLogger.logEventStarted("LineDown started.", System.currentTimeMillis());
                        }
                        if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                            cwpmsg.lineDown();
                            EventLogger.logEventStarted("LineDown started.", System.currentTimeMillis());
                        }
                    } else {
                        iv.setImageResource(R.mipmap.hal9000_offline);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (cwpmsg != null) {
            cwpmsg = ((CWPProvider) getActivity()).getMessaging();
            if (context instanceof CWPProvider) {
                cwpmsg.addObserver(this);
            }
        }
    }

    @Override
    public void onDetach() {
        if (cwpmsg != null) {
            Context context = getContext();
            if (context instanceof CWPProvider) {
                Log.d("TappingFragment","Calling " + ((CWPProvider) context).getMessaging());
                cwpmsg.deleteObserver(this);
            }
            cwpmsg = null;
        }
        super.onDetach();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg == CWProtocolListener.CWPEvent.ELineUp) {
            iv.setImageResource(R.mipmap.hal9000_up);
            EventLogger.logEventEnded("LineUp ended.");
        }
        if (arg == CWProtocolListener.CWPEvent.ELineDown) {
            iv.setImageResource(R.mipmap.hal9000_down);
            EventLogger.logEventEnded("LineDown ended.");
        }
        if (arg == CWProtocolListener.CWPEvent.EDisconnected) {
            iv.setImageResource(R.mipmap.hal9000_offline);
            EventLogger.logEventEnded("LineDown ended.");
        }
    }

    /*
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
}
