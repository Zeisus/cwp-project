package fi.oulu.tol.esde_2017_021.cwpclient021.model;

import java.io.IOException;
import java.util.Observable;

import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPControl;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPMessaging;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolImplementation;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolListener;

public class CWPModel extends Observable implements CWPMessaging, CWPControl, CWProtocolListener {

    private final CWProtocolImplementation cwpi = new CWProtocolImplementation(this);

    @Override
    public void lineUp() throws IOException {
        cwpi.lineUp();
        setChanged();
        notifyObservers(cwpi.getCurrentState());
    }

    @Override
    public void lineDown() throws IOException {
        cwpi.lineDown();
        setChanged();
        notifyObservers(cwpi.getCurrentState());
    }

    @Override
    public void connect(String serverAddr, int serverPort, int frequency) throws IOException {
        cwpi.connect(serverAddr, serverPort, frequency);
        setChanged();
        notifyObservers(cwpi.getCurrentState());
    }

    @Override
    public void disconnect() throws IOException {
        cwpi.disconnect();
        setChanged();
        notifyObservers(cwpi.getCurrentState());
    }

    @Override
    public boolean isConnected() {
        return cwpi.isConnected();
    }

    @Override
    public void setFrequency(int frequency) throws IOException, InterruptedException {
        cwpi.setFrequency(frequency);
        setChanged();
        notifyObservers();
    }

    @Override
    public int frequency() {
        return cwpi.frequency();
    }

    @Override
    public boolean lineIsUp() {
        return cwpi.lineIsUp();
    }

    @Override
    public void onEvent(CWProtocolListener.CWPEvent event, int param) {
        setChanged();
        notifyObservers(event);
    }
}
