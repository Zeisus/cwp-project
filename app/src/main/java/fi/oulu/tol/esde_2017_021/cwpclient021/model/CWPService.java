package fi.oulu.tol.esde_2017_021.cwpclient021.model;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import fi.oulu.tol.esde_2017_021.cwpclient021.MainActivity;
import fi.oulu.tol.esde_2017_021.cwpclient021.R;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPControl;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPMessaging;
import fi.oulu.tol.esde_2017_021.cwpclient021.CWPProvider;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWProtocolListener;

public class CWPService extends Service implements CWPProvider, Observer {

    private int serviceClients = 0;
    private CWPModel cwpm = null;
    private final CWPBinder cwpb = new CWPBinder();
    private Signaller signaller = null;

    public CWPService() {
    }

    public void startUsing() {
        if (++serviceClients == 1) {
            Log.d("CWPService", "startUsing() called! Number of clients: " +serviceClients);
            signaller = new Signaller();
            cwpm.addObserver(signaller);
        }
    }

    public void stopUsing() {
        if (--serviceClients == 0) {
            Log.d("CWPService", "stopUsing() called! Number of clients: " +serviceClients);
            signaller.update(cwpm, cwpm);
            cwpm.deleteObserver(signaller);
            signaller = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("CWPService", "onCreate() called!");
        if (cwpm == null) {
            cwpm = new CWPModel();
            cwpm.addObserver(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("CWPService", "onDestroy() called!");
        try {
            if (cwpm != null) {
                cwpm.disconnect();
            }
            cwpm = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NotificationManager notifyManager= (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notifyManager.cancelAll();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return cwpb;
    }

    @Override
    public CWPMessaging getMessaging() {
        return cwpm;
    }

    @Override
    public CWPControl getControl() {
        return cwpm;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (serviceClients == 0) {
            Intent resultIntent = new Intent(this, MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            if (arg == CWProtocolListener.CWPEvent.ELineUp) {

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_info_black_24dp)
                                .setContentTitle("CWP Client Event")
                                .setContentText("Line is up!");
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(1, mBuilder.build());

            } else if (arg == CWProtocolListener.CWPEvent.EDisconnected) {

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_info_black_24dp)
                                .setContentTitle("CWP Client Event")
                                .setContentText("Client disconnected!");
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(2, mBuilder.build());

            } else if (arg == CWProtocolListener.CWPEvent.EConnected) {

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_info_black_24dp)
                                .setContentTitle("CWP Client Event")
                                .setContentText("Client connected!");
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(3, mBuilder.build());

            }
        }
    }

    public class CWPBinder extends Binder {
        public Service getService() {
            return CWPService.this;
        }
    }
}
