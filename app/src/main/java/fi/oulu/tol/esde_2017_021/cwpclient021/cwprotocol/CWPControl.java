package fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol;

import java.io.IOException;
import java.util.Observer;

public interface CWPControl {
    int DEFAULT_FREQUENCY = -1;

    void addObserver(Observer observer);
    void deleteObserver(Observer observer);
    void connect(String serverAddr, int serverPort, int frequency) throws IOException;
    void disconnect()  throws IOException;
    boolean isConnected();
    // Channel management
    void setFrequency(int frequency) throws IOException, InterruptedException;
    int frequency();
}