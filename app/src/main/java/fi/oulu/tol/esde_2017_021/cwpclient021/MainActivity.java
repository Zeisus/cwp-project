package fi.oulu.tol.esde_2017_021.cwpclient021;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPControl;
import fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol.CWPMessaging;
import fi.oulu.tol.esde_2017_021.cwpclient021.model.CWPService;

public class MainActivity extends AppCompatActivity implements CWPProvider{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CWPService mService = null;
    private boolean mBound = false;

    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            CWPService.CWPBinder binder = (CWPService.CWPBinder) service;
            mService = (CWPService) binder.getService();
            mService.startUsing();
            mBound = true;
            Fragment f = mSectionsPagerAdapter.getItem(0);
            if (null != f) {
                ((TappingFragment) f).setMessaging(mService.getMessaging());
            }
            f = mSectionsPagerAdapter.getItem(1);
            if (null != f) {
                ((ControlFragment) f).setControl(mService.getControl());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Fragment f = mSectionsPagerAdapter.getItem(0);
            if (null != f) {
                ((TappingFragment)f).setMessaging(null);
            }
            f = mSectionsPagerAdapter.getItem(1);
            if (null != f) {
                ((ControlFragment)f).setControl(null);
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, CWPService.class);
        Log.d("MainActivity", "onStart() called!");
        if (!bindService(intent, mConnection, Context.BIND_AUTO_CREATE)) {
            Log.d("MainActivity", "bindService() returned false!");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop() called!");
        // Unbind from the service
        if (mBound) {
            mService.stopUsing();
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);

        Intent intent = new Intent(this, CWPService.class);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public CWPMessaging getMessaging() {
        if (mBound) {
            return mService.getMessaging();
        } else {
            return null;
        }
    }

    @Override
    public CWPControl getControl() {
        if (mBound) {
            return mService.getControl();
        } else {
            return null;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        TappingFragment tapper = null;
        ControlFragment control = null;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    if (tapper == null) {
                        tapper = new TappingFragment();
                    }
                    return tapper;
                case 1:
                    if (control == null) {
                        control = new ControlFragment();
                    }
                    return control;
            }
                return null;
        }

        @Override
        public int getCount() {
            // Show 2 pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.tapping_title);
                case 1:
                    return  getString(R.string.control_title);
            }
            return null;
        }
    }
}
