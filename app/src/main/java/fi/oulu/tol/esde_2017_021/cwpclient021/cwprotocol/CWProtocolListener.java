package fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol;

public interface CWProtocolListener {
    enum CWPEvent {EConnected, EChangedFrequency, ELineUp, ELineDown, EDisconnected}

    void onEvent(CWPEvent event, int param);
}