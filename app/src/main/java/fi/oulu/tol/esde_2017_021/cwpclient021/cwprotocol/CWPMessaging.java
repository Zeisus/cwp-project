package fi.oulu.tol.esde_2017_021.cwpclient021.cwprotocol;

import java.io.IOException;
import java.util.Observer;

public interface CWPMessaging {
    void addObserver(Observer observer);
    void deleteObserver(Observer observer);
    void lineUp() throws IOException;
    void lineDown() throws IOException;
    boolean isConnected();
    boolean lineIsUp();
}