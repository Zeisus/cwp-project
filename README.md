# Embedded Software Development Environments #

### Created by ###

Joni Jäntti

2316488

jonskutti@hotmail.com 

# Build #

Build and run the project in Android Studio.

* Minimum Android SDK API version: 19 (Android 4.4 - KitKat)
* Compile/Target SDK API version: 25

# Usage #

This application is used to connect to a continuous wave protocol server (default: **cwp.opimobi.com**) and send line up/down signals between all the connected devices on the same frequency.

After starting the app you are presented with the signaling button. This button is used to send and receive signals to and from the continuous wave protocol server. 

To connect to a server you have to use the settings menu located in the upper toolbar. In the "General" settings you can find the default server IP-address, default port and default frequency settings. Adjust these settings according to the CWP server and frequency you wish to connect to.

After setting up your default connection information you can return to the signaling button view. From this view you need to swipe to left to get access to the control view. Here you can use the CWP Connected/CWP Disconnected button to connect to the server. You can also switch frequency on the fly from this view as long as you are connected to a server. The default server has a CWP beacon functionality on the -1337 frequency that you can use for testing.

Now swipe back to the signaling button to interact with the server. By pressing the signaling button you can send signals to the server. You can also see the signals other clients are sending to the server.

# Testing #

The application can handle all of the problematic situations mentioned in the exercise description and demos without failing. These situations include:

 - Basic functionality maintained with multiple clients sending overlapping signals.
 - Frequency changing while connected to a server.
 - When running in the background the app is silent and gives notifications about CWP events.
 - Connecting without internet access will keep the client in disconnected state.
 - Turning internet connection (Wi-Fi) off while connected to a server will disconnect the client.

# Profiling #

Average times between initiating an event and the model actually updating the view on a Samsung Galaxy S4:

 - LineUp:  8.24 ms
 - LineDown:  27.46 ms
 - Connected:  88.92 ms
 - Disconnected:  31.70 ms

These delays were logged using EventLogger methods that are called when an action is first initiated and again when the event is updated by the model in the MVC architecture.

The delay times seem fair for use and they don't affect the user experience in any negative way.

Memory usage on a Samsung Galaxy S4:

 - Initial allocation: 
    19,17 MB (6,42 Free)
 - After connecting: 
    19,99 MB (5,61 Free)
 - After sending first LineUp and LineDown: 
    20,75 MB (4,86 Free)
 - After multiple signals sent and received: 
    21,06 MB (4,53 Free)
 - After moving the app to background while receiving signals: 
    18,90 MB (5,08 Free)
 - After pressing a signal notification to return to the app: 
    20,49 MB (3,49 Free)

Memory is being correctly released when the app moves to the background although the amount is not very significant.